﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Modal
{
    public class Tasks
    {
        int id;
        int userId;
        string title;
        string description;
        string duedate;
        string status;

        public int Id { get => id; set => id = value; }
        public int UserId { get => userId; set => userId = value; }
        public string Title { get => title; set => title = value; }
        public string Description { get => description; set => description = value; }
        public string Duedate { get => duedate; set => duedate = value; }
        public string Status { get => status; set => status = value; }
    }
}
