﻿using DataAccessLayer.Modal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Entities
{
    public class TaskOps
    {
        public bool CreateTask(Tasks task)
        {
            bool isValid = true;
            try
            {
                #region Variable
                    string fileDestination = ConfigurationManager.AppSettings["TaskFile"];
                    string fileName = fileDestination + "TaskFile.json";
                    List<int> idList = new List<int>();
                    List<Tasks> result = ReadTask(task.UserId);
                #endregion

                result.ForEach(x => idList.Add(x.Id));
                if (idList.Count == 0)
                    task.Id = 1;
                else
                    task.Id = idList.Max() + 1;
                result.Add(task);
                string json = JsonConvert.SerializeObject(result, Formatting.Indented);
                // Create the file, or overwrite if the file exists.
                using (FileStream fs = File.Create(fileName))
                {
                    byte[] info = new UTF8Encoding(true).GetBytes(json);
                    // Add some information to the file.
                    fs.Write(info, 0, info.Length);
                }
            }
            catch (Exception ex)
            {
                isValid = false;
            }
            return isValid;
        }

        public bool UpdateTask(Tasks task)
        {
            bool isValid = true;

            try
            {
                #region Variable
                string fileDestination = ConfigurationManager.AppSettings["TaskFile"];
                string fileName = fileDestination + "TaskFile.json";
                List<int> idList = new List<int>();
                List<Tasks> result = ReadTask(task.UserId);
                #endregion

                result.ForEach(x => {
                    if (x.Id == task.Id)
                    {
                        x.Title = task.Title;
                        x.UserId = task.UserId;
                        x.Description = task.Description;
                        x.Duedate = task.Duedate;
                        x.Status = task.Status;
                    }
                });
              
                string json = JsonConvert.SerializeObject(result, Formatting.Indented);
                // Create the file, or overwrite if the file exists.
                using (FileStream fs = File.Create(fileName))
                {
                    byte[] info = new UTF8Encoding(true).GetBytes(json);
                    // Add some information to the file.
                    fs.Write(info, 0, info.Length);
                }
            }
            catch (Exception ex)
            {
                isValid = false;
            }
            return isValid;
        }

        public List<Tasks> ReadTask(int user)
        {
            string fileDestination = ConfigurationManager.AppSettings["TaskFile"];
            string fileName = fileDestination + "TaskFile.json";
            List<Tasks> result = new List<Tasks>();
            string content = "";
            List<int> idList = new List<int>();
            if (File.Exists(fileName))
            {
                content = File.ReadAllText(fileName);
                result = JsonConvert.DeserializeObject<List<Tasks>>(content);
                if (result == null)
                {
                    result = new List<Tasks>();
                }
                result = result.FindAll(x => x.UserId == user);
            }
            else
            {
                // Create the file, or overwrite if the file exists.
                FileStream fs = File.Create(fileName);
            }
            return result;
        }

        public Tasks ReadTaskById(int taskid)
        {
            string fileDestination = ConfigurationManager.AppSettings["TaskFile"];
            string fileName = fileDestination + "TaskFile.json";
            List<Tasks> result = new List<Tasks>();
            string content = "";
            List<int> idList = new List<int>();
            if (File.Exists(fileName))
            {
                content = File.ReadAllText(fileName);
                result = JsonConvert.DeserializeObject<List<Tasks>>(content);
                if (result == null)
                {
                    result = new List<Tasks>();
                }
                result = result.FindAll(x => x.Id == taskid);
            }
            else
            {
                // Create the file, or overwrite if the file exists.
                FileStream fs = File.Create(fileName);
            }
            return result.FirstOrDefault();
        }

        public bool MarkAsComplete(int taskid)
        {
            Tasks task = ReadTaskById(taskid);
            task.Status = "Complete";
            UpdateTask(task);

            List<SubTasks> subTasks= new List<SubTasks>();
            SubTaskOps subTaskOps = new SubTaskOps();
            subTasks = subTaskOps.ReadTask(taskid);
            foreach (SubTasks subTask in subTasks)
            {
                subTask.Status = "Completed";
                subTaskOps.UpdateTask(subTask);
            }
            return true;
        }
    }
}
