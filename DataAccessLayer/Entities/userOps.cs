﻿using DataAccessLayer.Modal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;

namespace DataAccessLayer.Entities
{
    public class userOps
    {
        public bool CreateUser(Users users)
        {
            bool isValid = true;
            try
            {
                #region Variable
                string fileDestination = ConfigurationManager.AppSettings["UserFile"];
                string fileName = fileDestination + "UserFile.json";
                List<int> idList = new List<int>();
                List<string> emailList = new List<string>();
                List<Users> result = ReadUser();
                #endregion

                result.ForEach(x => {
                    idList.Add(x.Id);
                    emailList.Add(x.Email);
                });
                if (idList.Count == 0)
                    users.Id = 1;
                else
                    users.Id = idList.Max() + 1;
                if(!emailList.Contains(users.Email))
                    result.Add(users);
                string json = JsonConvert.SerializeObject(result, Formatting.Indented);
                // Create the file, or overwrite if the file exists.
                using (FileStream fs = File.Create(fileName))
                {
                    byte[] info = new UTF8Encoding(true).GetBytes(json);
                    // Add some information to the file.
                    fs.Write(info, 0, info.Length);
                }
            }
            catch (Exception ex)
            {
                isValid = false;
            }
            return isValid;
        }

        public bool UpdateUser(Users users)
        {
            bool isValid = true;

            try
            {
                #region Variable
                string fileDestination = ConfigurationManager.AppSettings["UserFile"];
                string fileName = fileDestination + "UserFile.json";
                List<int> idList = new List<int>();
                List<Users> result = ReadUser();
                #endregion

                result.ForEach(x => {
                    if (x.Id == users.Id)
                    {
                        x.Name = users.Name;
                        x.Email = users.Email;
                        x.Password = users.Password;
                    }
                });

                string json = JsonConvert.SerializeObject(result, Formatting.Indented);
                // Create the file, or overwrite if the file exists.
                using (FileStream fs = File.Create(fileName))
                {
                    byte[] info = new UTF8Encoding(true).GetBytes(json);
                    // Add some information to the file.
                    fs.Write(info, 0, info.Length);
                }
            }
            catch (Exception ex)
            {
                isValid = false;
            }
            return isValid;
        }

        public List<Users> ReadUser()
        {
            string fileDestination = ConfigurationManager.AppSettings["UserFile"];
            string fileName = fileDestination + "UserFile.json";
            List<Users> result = new List<Users>();
            string content = "";
            List<int> idList = new List<int>();
            if (File.Exists(fileName))
            {
                content = File.ReadAllText(fileName);
                result = JsonConvert.DeserializeObject<List<Users>>(content);
                if (result == null)
                {
                    result = new List<Users>();
                }
            }
            else
            {
                // Create the file, or overwrite if the file exists.
                FileStream fs = File.Create(fileName);
            }
            return result;
        }

        public Users ReadUserById(int userid)
        {
            string fileDestination = ConfigurationManager.AppSettings["UserFile"];
            string fileName = fileDestination + "UserFile.json";
            List<Users> result = new List<Users>();
            string content = "";
            List<int> idList = new List<int>();
            if (File.Exists(fileName))
            {
                content = File.ReadAllText(fileName);
                result = JsonConvert.DeserializeObject<List<Users>>(content);
                if (result == null)
                {
                    result = new List<Users>();
                }
                result = result.FindAll(x => x.Id == userid);
            }
            else
            {
                // Create the file, or overwrite if the file exists.
                FileStream fs = File.Create(fileName);
            }
            return result.FirstOrDefault();
        }

        public Users IsValidUser(string email, string password)
        {
            bool isValid = false;
            string fileDestination = ConfigurationManager.AppSettings["UserFile"];
            string fileName = fileDestination + "UserFile.json";
            List<Users> result = new List<Users>();
            string content = "";
            List<int> idList = new List<int>();
            if (File.Exists(fileName))
            {
                content = File.ReadAllText(fileName);
                result = JsonConvert.DeserializeObject<List<Users>>(content);
                if (result == null)
                {
                    result = new List<Users>();
                }
                result = result.FindAll(x => x.Email == email && x.Password == password);
                if (result.Count > 0)
                {
                    isValid = true;
                }
            }
            else
            {
                // Create the file, or overwrite if the file exists.
                FileStream fs = File.Create(fileName);
                isValid = false;
            }
            if (isValid == true)
                return result.FirstOrDefault();
            else
                return null;
        }
    }
}
