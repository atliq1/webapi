﻿using DataAccessLayer.Modal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Entities
{
    public class SubTaskOps
    {
        public bool CreateTask(SubTasks subtask)
        {
            bool isValid = true;
            try
            {
                #region Variable
                string fileDestination = ConfigurationManager.AppSettings["SubTaskFile"];
                string fileName = fileDestination + "SubTaskFile.json";
                List<int> idList = new List<int>();
                List<SubTasks> result = ReadTask(subtask.Taskid);
                #endregion

                result.ForEach(x => idList.Add(x.Id));
                if (idList.Count == 0)
                    subtask.Id = 1;
                else
                    subtask.Id = idList.Max() + 1;
                result.Add(subtask);
                string json = JsonConvert.SerializeObject(result, Formatting.Indented);
                // Create the file, or overwrite if the file exists.
                using (FileStream fs = File.Create(fileName))
                {
                    byte[] info = new UTF8Encoding(true).GetBytes(json);
                    // Add some information to the file.
                    fs.Write(info, 0, info.Length);
                }
            }
            catch (Exception ex)
            {
                isValid = false;
            }
            return isValid;
        }

        public bool UpdateTask(SubTasks subTasks)
        {
            bool isValid = true;

            try
            {
                #region Variable
                string fileDestination = ConfigurationManager.AppSettings["SubTaskFile"];
                string fileName = fileDestination + "SubTaskFile.json";
                List<int> idList = new List<int>();
                List<SubTasks> result = ReadTask(subTasks.Taskid);
                #endregion

                result.ForEach(x => {
                    if (x.Id == subTasks.Id)
                    {
                        x.Title = subTasks.Title;
                        x.Taskid = subTasks.Taskid;
                        x.Description = subTasks.Description;
                        x.Duedate = subTasks.Duedate;
                        x.Status = subTasks.Status;
                    }
                });
                string json = JsonConvert.SerializeObject(result, Formatting.Indented);
                // Create the file, or overwrite if the file exists.
                using (FileStream fs = File.Create(fileName))
                {
                    byte[] info = new UTF8Encoding(true).GetBytes(json);
                    // Add some information to the file.
                    fs.Write(info, 0, info.Length);
                }

                List<SubTasks> completedSubtask = result.FindAll(x => x.Status.ToUpper() == "Complete".ToUpper());

                if (result.Count() == completedSubtask.Count())
                {
                    TaskOps taskOps = new TaskOps();
                    Tasks taskItem = taskOps.ReadTaskById(subTasks.Id);
                    taskItem.Status = "Complete";
                    taskOps.UpdateTask(taskItem);
                }

            }
            catch (Exception ex)
            {
                isValid = false;
            }
            return isValid;
        }

        public List<SubTasks> ReadTask(int taskid)
        {
            string fileDestination = ConfigurationManager.AppSettings["SubTaskFile"];
            string fileName = fileDestination + "SubTaskFile.json";
            List<SubTasks> result = new List<SubTasks>();
            string content = "";
            List<int> idList = new List<int>();
            if (File.Exists(fileName))
            {
                content = File.ReadAllText(fileName);
                result = JsonConvert.DeserializeObject<List<SubTasks>>(content);
                if (result == null)
                {
                    result = new List<SubTasks>();
                }
                result = result.FindAll(x => x.Taskid == taskid);
            }
            else
            {
                // Create the file, or overwrite if the file exists.
                FileStream fs = File.Create(fileName);
            }
            return result;
        }

        public SubTasks ReadTaskById(int subtaskid)
        {
            string fileDestination = ConfigurationManager.AppSettings["SubTaskFile"];
            string fileName = fileDestination + "SubTaskFile.json";
            List<SubTasks> result = new List<SubTasks>();
            string content = "";
            List<int> idList = new List<int>();
            if (File.Exists(fileName))
            {
                content = File.ReadAllText(fileName);
                result = JsonConvert.DeserializeObject<List<SubTasks>>(content);
                if (result == null)
                {
                    result = new List<SubTasks>();
                }
                result = result.FindAll(x => x.Id == subtaskid);
            }
            else
            {
                // Create the file, or overwrite if the file exists.
                FileStream fs = File.Create(fileName);
            }
            return result.FirstOrDefault();
        }
        public bool MarkAsComplete(int subtaskid)
        {
            SubTasks subtask = ReadTaskById(subtaskid);
            subtask.Status = "Complete";
            UpdateTask(subtask);

            return true;
        }
    }
}
