﻿using DataAccessLayer.Modal;
using DataAccessLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    class Program
    {
        static void Main(string[] args)
        {
            //Users users = new Users();
            //users.Name = "Rajat";
            //users.Email = "rajat2928@gmail.com";
            //users.Password = "newuser";

            //userOps userOps = new userOps();
            //userOps.CreateUser(users);
            //userOps.UpdateUser(users);
            //Tasks task = new Tasks();
            //task.Title = "Badminton";
            //task.UserId = 1;
            //task.Description = "Playing Badminton";
            //task.Duedate = "20/02/2001";
            //task.Status = "Incomplete";

            //TaskOps taskOps = new TaskOps();
            //taskOps.CreateTask(task);
            //task = new Tasks();
            //task.Title = "Cricket";
            //task.UserId = 1;
            //task.Description = "Playing Cricket";
            //task.Duedate = "20/02/2001";
            //task.Status = "Complete";
            //taskOps.CreateTask(task);
            SubTasks task = new SubTasks();
            task.Title = "Badminton";
            task.Taskid = 1;
            task.Description = "Playing Badminton";
            task.Duedate = "20/02/2001";
            task.Status = "Incomplete";

            SubTaskOps taskOps = new SubTaskOps();
            taskOps.CreateTask(task);
            task = new SubTasks();
            task.Title = "Cricket";
            task.Taskid = 1;
            task.Description = "Playing Cricket";
            task.Duedate = "20/02/2001";
            task.Status = "Incomplete";
            taskOps.CreateTask(task);
        }
    }
}
