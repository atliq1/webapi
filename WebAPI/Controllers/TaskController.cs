﻿using DataAccessLayer.Entities;
using DataAccessLayer.Modal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class TaskController : ApiController
    {
        // GET: api/Task
        [Route("api/Task/GetAllTask/{userid}")]
        public List<Tasks> GetAllTask(int userid)
        {
            TaskOps taskOps = new TaskOps();
            return taskOps.ReadTask(userid);
        }

        // GET: api/Task/5
        [Route("api/Task/GetTaskbyID/{taskId}")]
        public Tasks GetTaskbyID(int taskId)
        {
            TaskOps taskOps = new TaskOps();
            return taskOps.ReadTaskById(taskId);
        }

        // POST: api/Task
        public bool Post([FromBody]Tasks tasks)
        {
            TaskOps taskOps = new TaskOps();
            return taskOps.CreateTask(tasks);
        }

        // PUT: api/Task/5
        public bool Put([FromBody]Tasks tasks)
        {
            TaskOps taskOps = new TaskOps();
            return taskOps.UpdateTask(tasks);
        }

        // PUT: api/Task/5
        [Route("api/Task/GetMarkasComplete/{taskId}")]
        public bool GetMarkasComplete(int taskid)
        {
            TaskOps taskOps = new TaskOps();
            return taskOps.MarkAsComplete(taskid);
        }
    }
}
