﻿using DataAccessLayer.Entities;
using DataAccessLayer.Modal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class SubTaskController : ApiController
    {
        // GET: api/SubTask
        [Route("api/SubTask/Get/{taskId}")]
        public List<SubTasks> Get(int taskId)
        {
            SubTaskOps subTaskOps = new SubTaskOps();
            return subTaskOps.ReadTask(taskId);
        }

        // GET: api/SubTask/5
        [Route("api/SubTask/GetbySubtaskID/{subTaskId}")]
        public SubTasks GetbySubtaskID(int subTaskId)
        {
            SubTaskOps subTaskOps = new SubTaskOps();
            return subTaskOps.ReadTaskById(subTaskId);
        }

        // POST: api/SubTask
        public bool Post([FromBody]SubTasks subTasks)
        {
            SubTaskOps subTaskOps = new SubTaskOps();
            return subTaskOps.CreateTask(subTasks);
        }

        // PUT: api/SubTask/5
        public bool Put(int id, [FromBody]SubTasks subTasks)
        {
            SubTaskOps subTaskOps = new SubTaskOps();
            return subTaskOps.UpdateTask(subTasks);
        }
        // PUT: api/Task/5
        [Route("api/SubTask/GetMarkasComplete/{subtaskid}")]
        public bool GetMarkasComplete(int subtaskid)
        {
            SubTaskOps subTaskOps = new SubTaskOps();
            return subTaskOps.MarkAsComplete(subtaskid);
        }
    }
}
