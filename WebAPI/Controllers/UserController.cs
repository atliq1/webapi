﻿using DataAccessLayer.Entities;
using DataAccessLayer.Modal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class UserController : ApiController
    {
        // GET: api/User
        public List<Users> Get()
        {
            userOps userOps = new userOps();
            return userOps.ReadUser();
        }

        // GET: api/User/5
        public Users Get(int id)
        {
            userOps userOps = new userOps();
            return userOps.ReadUserById(id);
        }
        // GET: api/User/5/4
        [Route("api/User/{email}/{password}")]
        public Users Get(string email, string password)
        {
            userOps userOps = new userOps();
            return userOps.IsValidUser(email, password);
        }
        // POST: api/User
        public bool Post([FromBody]Users user)
        {
            userOps userOps = new userOps();
            return userOps.CreateUser(user);
        }

        // PUT: api/User/5
        public bool Put([FromBody]Users user)
        {
            userOps userOps = new userOps();
            return userOps.UpdateUser(user);
        }


    }
}
